import { Component, OnInit } from '@angular/core'
import { MatTableModule, MatTable } from '@angular/material/table'
import { SelectionModel } from '@angular/cdk/collections'
import { MatTableDataSource } from '@angular/material'
import { HttpClient, HttpHeaders } from '@angular/common/http'

export interface Food {
  calories: number
  carbs: number
  fat: number
  name: string
  protein: number
}
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent {
  data: Food[] = []
  dataSource = new MatTableDataSource(this.data)
  calories: number
  carbs: number
  fat: number
  name: string
  protein: number

  constructor(private http: HttpClient) {}

  ngOnInit() {
    //retrieve token, because it is necessary to acces the food API
    const token = localStorage.getItem('myToken')
    const headers = new HttpHeaders({ Authorization: 'bearer ' + token })

    this.http.get('http://localhost:3000/foods', { headers }).subscribe(d => {
      this.dataSource = new MatTableDataSource(d as Food[])
    })
  }

  displayedColumns: string[] = ['name', 'calories', 'fat', 'carbs', 'protein']

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
  }

  postFood() {
    console.log('trying to create a new food item!')

    //retrieve token, because it is necessary to acces the food API
    const token = localStorage.getItem('myToken')
    const headers = new HttpHeaders({
      Authorization: 'bearer ' + token,
      'Content-Type': 'application/json'
    })

    const body = {
      name: this.name,
      calories: this.calories,
      fat: this.fat,
      carbohydrates: this.carbs,
      protein: this.protein
    }

    this.http
      .post('http://localhost:3000/foods', JSON.stringify(body), {
        headers
      })
      .subscribe(
        d => console.log(d),
        e => console.error(e)
      )

    //update data yo
    this.ngOnInit()
  }
}
