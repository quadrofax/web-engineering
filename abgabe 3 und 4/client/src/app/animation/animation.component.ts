import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.css'],
  animations: [
    trigger('easy', [
    state('blau',
    style({fill: 'black', transform: 'translateX(0)',
   })),
    state('grun',
    style({ fill: 'red', transform: 'translateZ(-800px)' + 'rotate(-30000deg)'+'rotateX(-300deg)',opacity: 0
   })),
   transition('* <=> grun', animate(5000))
  ])]
})
export class AnimationComponent {
state = 'blau';

onAnimate() {
  this.state === 'blau' ? this.state = 'grun' : this.state = 'blau';
}

}

