import { Component, OnInit, ɵConsole } from "@angular/core";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Token } from "@angular/compiler/src/ml_parser/lexer";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  constructor(private router: Router, private http: HttpClient) {}
  username: string;
  password: string;

  ngOnInit() {}
  login(): void {
    const body = { email: "admin", pass: "webengineeringfh" };
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    this.http
      .post("http://localhost:3000/rpc/login", JSON.stringify(body), {
        headers
      })
      .subscribe(data => {
        let token = null;

        if (data[0]) {
          token = data[0].token;
        }

        if (token) {
          localStorage.setItem("myToken", token);
          console.log("token stored yo");
          this.router.navigate(["user"]);
        } else {
          alert("INVALID CREDENTIALS");
        }
        console.log(data[0].token);
      });
  }
}
