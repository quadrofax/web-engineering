create extension if not exists pgcrypto;
CREATE EXTENSION pgjwt;

create schema api;

CREATE TYPE jwt_token AS (
  token text
);

CREATE FUNCTION jwt_test() RETURNS public.jwt_token AS $$
  SELECT public.sign(
    row_to_json(r), 'reallyreallyreallyreallyverysafe'
  ) AS token
  FROM (
    SELECT
      'my_role'::text as role,
      extract(epoch from now())::integer + 300 AS exp
  ) r;
$$ LANGUAGE sql;

---HEHEHOHO

-- We put things inside the api schema to hide
-- them from public view. Certain public procs/views will
-- refer to helpers and tables inside.
create schema if not exists api;

CREATE TYPE api.jwt_token AS (
  token text
);

create table if not exists
api.users (
  email    text primary key,
  pass     text not null check (length(pass) < 512),
  role     name not null check (length(role) < 512)
);

---part2

create function
api.check_role_exists() returns trigger as $$
begin
  if not exists (select 1 from pg_roles as r where r.rolname = new.role) then
    raise foreign_key_violation using message =
      'unknown database role: ' || new.role;
    return null;
  end if;
  return new;
end
$$ language plpgsql;

create constraint trigger ensure_user_role_exists
  after insert or update on api.users
  for each row
  execute procedure api.check_role_exists();

---part3

create function
api.encrypt_pass() returns trigger as $$
begin
  if tg_op = 'INSERT' or new.pass <> old.pass then
    new.pass = crypt(new.pass, gen_salt('bf'));
  end if;
  return new;
end
$$ language plpgsql;

create trigger encrypt_pass
  before insert or update on api.users
  for each row
  execute procedure api.encrypt_pass();

---part4

create function
api.user_role(email text, pass text) returns name
  language plpgsql
  as $$
begin
  return (
  select role from api.users
   where users.email = user_role.email
     and users.pass = crypt(user_role.pass, users.pass)
  );
end;
$$;

---part5

-- login should be on your exposed schema
create function api.login(email text, pass text) returns api.jwt_token as $$
declare
  _role name;
  result api.jwt_token;
begin
  -- check email and password
  select api.user_role(email, pass) into _role;
  if _role is null then
    raise invalid_password using message = 'invalid user or password';
  end if;

  select sign(
      row_to_json(r), 'reallyreallyreallyreallyverysafe'
    ) as token
    from (
      select _role as role, login.email as email,
         extract(epoch from now())::integer + 60*60 as exp
    ) r
    into result;
  return result;
end;
$$ language plpgsql security definer;

---MY TABLES



create table api.todos
(
  id serial primary key,
  done boolean not null default false,
  task text not null,
  due timestamptz
);

insert into api.todos
  (task)
values
  ('finish tutorial 0'),
  ('pat self on back');

create table api.foods
(
  id serial primary key,
  name text not null,
  calories integer not null default 0,
  fat integer not null default 0,
  protein integer not null default 0,
  carbohydrates integer not null default 0
);

insert into api.foods (name, calories, fat, protein, carbohydrates) VALUES ('Yogurt', 159, 6, 24, 4);
insert into api.foods (name, calories, fat, protein, carbohydrates) VALUES ('Sandwich', 237, 9, 37, 4);
insert into api.foods (name, calories, fat, protein, carbohydrates) VALUES ('Eclairs', 262, 16, 24, 6);
insert into api.foods (name, calories, fat, protein, carbohydrates) VALUES ('Cupcakes', 305, 4, 67, 4);
insert into api.foods (name, calories, fat, protein, carbohydrates) VALUES ('Gingerbreads', 356, 16, 49, 4);


--create role web_anon nologin;

--grant usage on schema api to web_anon;
--grant select on api.todos to web_anon;

-- the names "anon" and "authenticator" are configurable and not
-- sacred, we simply choose them for clarity
create role anon noinherit;
create role authenticator noinherit;
-- grant anon to authenticator;

grant execute on function api.login(text,text) to anon;
-- grant usage on schema api to anon;
grant select on api.users to authenticator;
grant select on api.todos to authenticator;
grant select on api.foods to authenticator;


GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE api.todos TO authenticator;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE api.foods TO authenticator;


INSERT INTO api.users VALUES ('admin', 'webengineeringfh', 'authenticator');

